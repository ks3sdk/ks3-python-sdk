# KS3 Python SDK 快速入门

---

用户常用操作，创建存储空间，下载文件，上传文件等
- [KS3 python SDK 快速入门](#ks3-python-sdk-快速入门)
  - [创建存储空间](#创建存储空间)
  - [列举存储空间](#列举存储空间)
  - [下载文件](#下载文件)
  - [上传文件](#上传文件)
  - [删除文件](#删除文件)
  - [列举文件](#列举文件)

## 创建存储空间

_创建一个新的 Bucket_

在建立了连接后，可以创建一个 Bucket。Bucket 在 ks3 中是一个用于储存 key/value 的容器。用户可以将所有的数据存储在一个 Bucket 里，也可以为不同种类数据创建相应的 Bucket。

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='YOUR_REGION_ENDPOINT')
    # 这里如果出现409 conflict错误，说明请求的bucket name有冲突，因为bucket name是全局唯一的
    b = c.create_bucket("<YOUR_BUCKET_NAME>")

## 列举存储空间

_列出客户所有的 Bucket 信息_

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='YOUR_REGION_ENDPOINT')
    buckets = c.get_all_buckets()
    for b in buckets:
    	print(b.name)

## 下载文件

下载 Object，并且保存到文件中

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='YOUR_REGION_ENDPOINT')

    key_name = "YOUR_KEY_NAME"
    b = c.get_bucket("<YOUR_BUCKET_NAME>")
    k = b.get_key(key_name)
    k.get_contents_to_filename("<SAVED_FILE_PATH>")

下载 Object，并且作为字符串返回

    #打印object内容
    s = k.get_contents_as_string().decode()
    print(s)

## 上传文件

将指定目录下某一个文件上传，同时可以指定文件 ACL

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='YOUR_REGION_ENDPOINT')

    b = c.get_bucket("<YOUR_BUCKET_NAME>")
    k = b.new_key("<YOUR_KEY_NAME>")
    ret=k.set_contents_from_filename("<YOUR_SOURCE_FILE_PATH>")
    if ret and ret.status == 200:
        print("上传成功")

将字符串作为 value 上传

    k.set_contents_from_string('<YOUR_FILE_CONTENTS>')

## 删除文件

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='YOUR_REGION_ENDPOINT')

    b=c.get_bucket("<YOUR_BUCKET_NAME>")
    b.delete_key("<YOUR_KEY_NAME>")

## 列举文件

_列举 Bucket 内的文件或者目录_

    from ks3.connection import Connection
    from ks3.prefix import Prefix
    from ks3.key import Key
    
    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='YOUR_REGION_ENDPOINT')

    b = c.get_bucket("<YOUR_BUCKET_NAME>")
    keys = b.list()
    for k in keys:
      print('object:', k)

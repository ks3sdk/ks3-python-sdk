# KS3 SDK for Python 文档

---

## 目录
- [KS3 SDK for Python 文档](#ks3-sdk-for-python-文档)
  - [目录](#目录)
  - [文档直达](#文档直达)
  - [SDK 介绍及使用](#sdk-介绍及使用)
    - [Service 操作](#service-操作)
      - [列举 Bucket](#列举-bucket)
    - [Bucket 操作](#bucket-操作)
      - [创建 Bucket](#创建-bucket)
      - [删除 Bucket](#删除-bucket)
      - [Bucket 访问权限管理](#bucket-访问权限管理)
      - [Bucket 策略管理](#bucket-策略管理)
    - [Object 操作](#object-操作)
      - [获取 Object 元信息](#获取-object-元信息)
      - [下载 Object 数据](#下载-object-数据)
      - [上传 Object 数据](#上传-object-数据)
        - [上传回调](#上传回调)
        - [抓取网络资源上传](#抓取网络资源上传)
      - [分块上传](#分块上传)
        - [低频存储方式上传](#低频存储方式上传)
        - [列出指定上传任务中所有已上传的块](#列出指定上传任务中所有已上传的块)
        - [列出 Bucket 内所有正在进行的分块上传任务](#列出-bucket-内所有正在进行的分块上传任务)
      - [复制 Object](#复制-object)
      - [删除 Object](#删除-object)
      - [列举 Bucket 内的文件或者目录](#列举-bucket-内的文件或者目录)
      - [获得 Object 访问权限](#获得-object-访问权限)
      - [设置 Object 访问权限](#设置-object-访问权限)
      - [生成下载外链地址](#生成下载外链地址)
      - [生成文件上传外链地址](#生成文件上传外链地址)
      - [修改 Object 元数据](#修改-object-元数据)
      - [修改 Object 存储类型](#修改-object-存储类型)
      - [归档 Object 解冻](#归档-object-解冻)
## 文档直达

- [快速入门](GUIDE.md)
- [加密上传——服务端加密和客户端加密](ENCRYPTION.md)
- [多线程分块上传下载](/examples/multi.py)
- [金山云官网 Python SDK 文档](https://docs.ksyun.com/documents/965)

## SDK 介绍及使用
### Service 操作

#### 列举 Bucket

_列出客户所有的 Bucket 信息_

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    buckets = c.get_all_buckets()
    for b in buckets:
    	print(b.name)

### Bucket 操作

#### 创建 Bucket

_创建一个新的 Bucket_

在建立了连接后，可以创建一个 Bucket。Bucket 在 s3 中是一个用于储存 key/value 的容器。用户可以将所有的数据存储在一个 Bucket 里，也可以为不同种类数据创建相应的 Bucket。

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.create_bucket('<YOUR_BUCKET_NAME>')

注：这里如果出现 409 conflict 错误，说明请求的 bucket_name 有冲突，因为 Bucket 名称 是全局唯一的

#### 删除 Bucket

_删除指定 Bucket_

删除一个 Bucket 可以通过 delete_bucket 方法实现。

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    c.delete_bucket('<YOUR_BUCKET_NAME>')

如果 Bucket 下面存在 key，那么需要首先删除所有 key

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')
    for k in b.list():
        k.delete()
    c.delete_bucket('<YOUR_BUCKET_NAME>')

#### Bucket 访问权限管理

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')
    # 获取 Bucket 访问权限，并打印
    policy = b.get_acl()
    for grant in policy.acl.grants:
    	print(grant.permission, grant.display_name, grant.email_address, grant.id)

    # 设置bucket的权限, private or public-read or public-read-write
    b.set_acl("public-read")

#### Bucket 策略管理

对 Bucket 进行空间策略配置，支持添加、查询、删除

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    bucket = c.get_bucket('<YOUR_BUCKET_NAME>')
    # 设置空间策略
    bucket.set_bucket_policy(
        policy='{"Statement":[{"Resource":["krn:ksc:ks3:::jiangran123","krn:ksc:ks3:::jiangran123/*"],"Principal":{"KSC":["krn:ksc:iam::32432423:root"]},"Action":["ks3:*"],"Effect":"Allow"}]}')
    # 获取空间策略
    # policy = bucket.get_bucket_policy()
    # 删除空间策略
    # bucket.delete_bucket_policy()

参见：
- [空间策略](https://docs.ksyun.com/documents/5177)
- [权限概述](https://docs.ksyun.com/documents/5170)

### Object 操作

#### 获取 Object 元信息

获取 Object 元数据信息（大小、最后更新时间等）

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')
    k = b.get_key('<YOUR_KEY_NAME>')
    if k:
         print(k.name, k.size, k.last_modified)

#### 下载 Object 数据

下载 Object，并且作为字符串返回

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')
    try:
        k = b.get_key('<YOUR_KEY_NAME>')
        s = k.get_contents_as_string().decode()
        print(s)
    except:
        pass # 异常处理

下载 Object，并且保存到文件中

    #保存到文件
    k.get_contents_to_filename("SAVED_FILE_PATH")

#### 上传 Object 数据

将指定目录下某一个文件上传，同时可以指定文件 ACL

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    try:
        b = c.get_bucket('<YOUR_BUCKET_NAME>')
        k = b.new_key('<YOUR_KEY_NAME>')
        #object policy : 'private' or 'public-read'
        ret=k.set_contents_from_filename("<YOUR_SOURCE_FILE_PATH>", policy="private")
        if ret and ret.status == 200:
        	print("上传成功")
    except:
        pass #异常处理

将字符串作为 value 上传

    k.set_contents_from_string('<YOUR_FILE_CONTENTS>')

以低频存储方式上传

```
# x-kss-storage-class有效值为"STANDARD"、"STANDARD_IA"。"STANDARD"表示标准存储，"STANDARD_IA"表示低频存储，如果不指定，默认为标准存储。
headers = {"x-kss-storage-class": "STANDARD_IA"}
resp = k.set_contents_from_filename("<YOUR_SOURCE_FILE_PATH>", policy="private", headers=headers)
```

##### 上传回调

需要设置`x-kss-callbackurl`和`x-kss-callbackbody`请求头，详见文档[上传回调处理](https://docs.ksyun.com/documents/956)。

```
headers = {"x-kss-callbackurl": "Callback_URL", "x-kss-callbackbody":"objectKey=${key}&etag=${etag}&uid=123"}
ret = k.set_contents_from_filename("<YOUR_SOURCE_FILE_PATH>", headers=headers)
```

##### 抓取网络资源上传

x-kss-acl 可根据需要设定公开读 public-read，当未设置时为私密

    bucket = conn.get_bucket('<YOUR_BUCKET_NAME>')
    bucket.fetch_object('www-logo', source_url='http://fe.ksyun.com/project/resource/img/www-logo.png', headers={'x-kss-acl': 'public-read'})

参见：[金山云官方文档-Put Object Fetch](https://docs.ksyun.com/documents/949)

#### 分块上传

如果你想上传一个大文件，你可以将它分成几个小份上传，KS3 会按照顺序把它们合成一个最终的 object。通过 SDK 进行分块上传能够保障数据传到 KS3 的正确性，无需另外做数据校验。

整个过程需要几步来完成，下面的 demo 程序是通过 Python 的 FileChunkIO 模块来实现的。所以可能需要首先运行 pip install FileChunkIO 来安装。

    import math, os
    from ks3.connection import Connection
    from filechunkio import FileChunkIO


    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')

    source_path = 'SOURCE_FILE_PATH'
    source_size = os.stat(source_path).st_size

    # 获取初始化的 uploadId，之后的操作中将会用到
    # 此处 os.path.basename(source_path) 可以替换为需要设置的 objectKey
    mp = b.initiate_multipart_upload(os.path.basename(source_path))
    # 举例以 50 MiB 为分块大小
    chunk_size = 52428800
    chunk_count = int(math.ceil(source_size*1.0 / chunk_size*1.0))

    # 通过 FileChunkIO 将文件分片
    try:
        for i in range(chunk_count):
            offset = chunk_size * i
            bytes = min(chunk_size, source_size - offset)
            with FileChunkIO(source_path, 'r', offset=offset, bytes=bytes) as fp:
                mp.upload_part_from_file(fp, part_num=i + 1)
        # 发送请求，合并分片，完成分片上传
        ret = mp.complete_upload()
        if ret and ret.status == 200:
            print("上传成功")
    except:
        pass

##### 低频存储方式上传
以低频存储方式上传，需要在 initiate_multipart_upload 阶段设置

```
# x-kss-storage-class有效值为"STANDARD"、"STANDARD_IA"。"STANDARD"表示标准存储，"STANDARD_IA"表示低频存储，如果不指定，默认为标准存储。
headers = {"x-kss-storage-class": "STANDARD_IA"}
mp = b.initiate_multipart_upload('<YOUR_KEY_NAME>', policy="private", headers=headers)
```

##### 列出指定上传任务中所有已上传的块

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')
    mp = b.initiate_multipart_upload('<YOUR_KEY_NAME>')
    # part_number_marker 指定应该从哪个块开始列举，只有比设定值大的块才会被列举
    for p in mp.get_all_parts(part_number_marker=2):
        print('part_number:%s' % p.part_number)

##### 列出 Bucket 内所有正在进行的分块上传任务

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')
    # 列举 Bucket 中所有的已上传分片信息
    for p in b.list_multipart_uploads():
        print('uploadId:%s,key:%s' % (p.id, p.key_name))
        for i in p:
            print(i.part_number, i.size, i.etag, i.last_modified)
#### 复制 Object

将源 Bucket 下的文件复制到目标 Bucket 下（需要对源 Bucket 下的文件具有读权限）

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    try:
        b = c.get_bucket('<YOUR_BUCKET_NAME>')
        b.copy_key('<YOUR_DST_KEY_NAME>', '<YOUR_SRC_BUCKET_NAME>', '<YOUR_SRC_KEY_NAME>')
    except:
        pass

#### 删除 Object

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    try:
        b=c.get_bucket('<YOUR_BUCKET_NAME>')
        b.delete_key('<YOUR_KEY_NAME>')
    except:
        pass

#### 列举 Bucket 内的文件或者目录

    from ks3.connection import Connection
    from ks3.prefix import Prefix
    from ks3.key import Key

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')
    keys = b.list()
    for k in keys:
        if isinstance(k, Key):
            print('file:%s' % k.name)
        elif isinstance(k, Prefix)
            print('dir:%s' % k.name)

_列举 Bucket 内指定前缀的文件_

    keys = b.list(prefix="PREFIX")

_列举 Bucket 内以指定分隔符分组的文件_

    # delimiter 为对文件名称进行分组的字符
    keys = b.list(delimiter='/') # delimiter为空时，默认为'/'
    # 返回结果中 delimiter 分隔符之前的字符会放入 commonPrefixes 中，可以类比理解为文件夹

_列举 Bucket 内指定前缀的文件以及指定时间区间的文件_

    # startTime = 1625460400 这个时间之前的数据
    # endTime   = 1625460400 这个时间之后的数据
    # startTime = 1525460400 ，endTime=1625460400 这个时间中间的数据
    keys = b.listObjects(filename="/Users/xxx/a.txt", prefix="local/load", endTime=1625460400)

#### 获得 Object 访问权限

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')
    policy = b.get_acl('<YOUR_KEY_NAME>')
    print(policy.to_xml())

#### 设置 Object 访问权限

    # object policy : private | public-read | public-read-write
    b.set_acl("public-read", test_key)

#### 生成下载外链地址

对私密属性的文件生成下载外链（该链接具有时效性）

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket('<YOUR_BUCKET_NAME>')
    k = b.get_key('<YOUR_KEY_NAME>')
    if k:
        url = k.generate_url(60) # 60s 后该链接过期
        print(url)

指定时间戳过期

    k.generate_url(1492073594, expires_in_absolute=True) # 1492073594为Unix Time

#### 生成文件上传外链地址

生成文件上传外链地址（该链接具有时效性）

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = c.get_bucket(bucket_name)
    # 新建对象key
    k = b.new_key(key_name)
    if k:
        url = k.get_presigned_url(60) # 60s 后该链接过期
        print(url)

指定时间戳过期

    k.get_presigned_url(1492073594, expires_in_absolute=True) # 1492073594为Unix Time


#### 修改 Object 元数据

    from ks3.connection import Connection

    conn = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = conn.get_bucket('<DST_BUCKET_NAME>')
    b.copy_key('<DST_KEY_NAME>', '<SRC_BUCKET_NAME>', '<SRC_KEY_NAME>', headers={'content-type':'text/plain', 'x-kss-metadata-directive':'REPLACE'})

#### 修改 Object 存储类型

    from ks3.connection import Connection

    conn = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    b = conn.get_bucket('<DST_BUCKET_NAME>')
    b.copy_key('<DST_KEY_NAME>', '<SRC_BUCKET_NAME>', '<SRC_KEY_NAME>', headers={'x-kss-storage-class':'STANDARD_IA'})


#### 归档 Object 解冻

此接口用于对归档 Object 进行解冻

    from ks3.connection import Connection

    c = Connection('<YOUR_ACCESS_KEY>', '<YOUR_SECRET_KEY>', host='<YOUR_REGION_ENDPOINT>', is_secure=False, domain_mode=False)
    bucket = c.get_bucket('<YOUR_BUCKET_NAME>')
    restore_result = bucket.restore_object("<YOUR_KEY_NAME>")
